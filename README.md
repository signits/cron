cron
=========

This role ensures that cron and anacron are installed. Further it changes permissions and owners of related configuration files, to comply with the CIS Benchmark

Requirements
------------

None

Role Variables
--------------

enable_cron: true - [enables or disables the role]

Dependencies
------------

None

License
-------

MIT

Author Information
------------------

signits@acme.dev